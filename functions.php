<?php
session_start();
$mysql = mysqli_connect('188.240.210.8', 'root', 'scoalait123', 'web-04_iulia');

function queryInsert($queryString)
{
    global $mysql;
    $query = mysqli_query($mysql, $queryString);

    if ($query === false) {
        die('Eroare query: ' . $queryString . "MySQL error: " . mysqli_error($mysql));
    }
    if ($query === true) {
        return mysqli_insert_id($mysql);
    }


    return mysqli_fetch_all($query, MYSQLI_ASSOC);
}

function query($queryString)
{
    global $mysql;
    $query = mysqli_query($mysql, $queryString);

    if ($query === false) {
        die('Eroare query: ' . $queryString . "MySQL error: " . mysqli_error($mysql));
    }

    return mysqli_fetch_all($query, MYSQLI_ASSOC);
}

function showArticle($article2, $key)
{
    if ($key % 2 == 0) {
        ?>
        <div class="row">
            <div class="col-12">
                <img src="<?php echo $article2['picture']; ?>" class="float-left" id="articlePhoto">
                <h3><?php echo $article2['title']; ?></h3>
                <?php echo $article2['contents'];
                $authors = query("SELECT *                                                                               
                                        FROM authors WHERE author_id='" . $article2['author_id'] . "'");
                ?>
                <p>
                    <?php
                foreach ($authors as $author) {
                    echo "Author : " . $author['firstName'] . ' ' . $author['lastName'];
                } ?> &nbsp; &nbsp; &nbsp;
                <?php $categories = query("SELECT *                                                                      
                                        FROM categories WHERE category_id='" . $article2['category_id'] . "'");
                foreach ($categories as $category) {
                    echo "Category : " . $category['name'];
                }

                ?>&nbsp; &nbsp; &nbsp;
                <?php echo "Posted on : " . $article2['creatingDate']; ?>
                </p>
                <h4>What is the readers'opinion ?</h4>
                <?php
                $comments = query("SELECT *                                                                                  
             FROM comment_admin                                                                                          
             WHERE status='visible' AND article_id='" . $article2['id'] . "' ");
                foreach ($comments as $comment) {
                    ?>
                    <?php
                    echo "Author: " . $comment['name'] . ' ' . $comment['surname'];
                    ?>
                    <?php
                    echo "Comment: " . $comment['comment'];
                    ?>
                    <?php
                } ?>
                <div class="row">
                    <div class="col-12">
                        <a href="addComment.php?article_id=<?php echo $article2['id']; ?>"
                           id="comment">+Add Comment</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else {
        ?>

        <div class="row">
            <div class="col-12">
                <img src="<?php echo $article2['picture']; ?>" class="float-right" id="articlePhoto">
                <h3><?php echo $article2['title']; ?></h3>
                <?php echo $article2['contents'];
                $authors = query("SELECT *                                                                                      
                                        FROM authors WHERE author_id='" . $article2['author_id'] . "'");
                ?>
                <p>
                    <?php
                foreach ($authors as $author) {
                    echo "Author : " . $author['firstName'] . ' ' . $author['lastName'];
                } ?> &nbsp; &nbsp; &nbsp;
                <?php $categories = query("SELECT *                                                                             
                                        FROM categories WHERE category_id='" . $article2['category_id'] . "'");
                foreach ($categories as $category) {
                    echo "Category : " . $category['name'];
                }

                ?>&nbsp; &nbsp; &nbsp;
                <?php echo "Posted on : " . $article2['creatingDate']; ?>
                </p>
                <h4>What is the readers'opinion ?</h4>
                <?php
                $comments = query("SELECT *                                                                                         
             FROM comment_admin                                                                                                 
             WHERE status='visible' AND article_id='" . $article2['id'] . "' ");
                foreach ($comments as $comment) {
                    ?>
                    <?php
                    echo "Author: " . $comment['name'] . ' ' . $comment['surname'];
                    ?>
                    <?php
                    echo "Comment: " . $comment['comment'];
                    ?>
                    <?php
                } ?>
                <div class="row">
                    <div class="col-12">
                        <a href="addComment.php?article_id=<?php echo $article2['id']; ?>"
                           id="comment">+Add Comment</a>
                    </div>
                </div>
            </div>
        </div>

        <?php
    }
}


function showArticleOnIndex($article, $key)
{
    if ($key % 2 == 1) {
        ?>

        <div class="row">
            <div class="col-12">
            <a href="article.php?id=<?php echo $article['id']; ?>"><img src="<?php echo $article['picture']; ?>"
                                                                        class="float-right" id="articlePhoto"></a>

            <h2 id="colorFont"> <?php echo $article['title']; ?></h2>
            <p>
                <?php
                echo substr($article['contents'], 0, 250); ?><a
                        href="article.php?id=<?php echo $article['id']; ?>">...Read more</a>
            </p>
            <p>  <?php $authors = query("SELECT *
                                        FROM authors WHERE author_id='" . $article['author_id'] . "'");
                foreach ($authors as $author) {
                    echo "Author : " . $author['firstName'] . ' ' . $author['lastName'];
                } ?> &nbsp; &nbsp; &nbsp;
                <?php $categories = query("SELECT *
                                        FROM categories WHERE category_id='" . $article['category_id'] . "'");
                foreach ($categories as $category) {
                    echo "Category : " . $category['name'];
                } ?>
                &nbsp; &nbsp; &nbsp;
                <?php echo "Posted on : " . $article['creatingDate']; ?>
            </p>

    </div>
</div>
        <?php
    } else {
        ?>

        <div class="row">
        <div class="col-12">
            <a href="article.php?id=<?php echo $article['id']; ?>"><img src="<?php echo $article['picture']; ?>"
                                                                        class="float-left" id="articlePhoto"></a>
            <h2 id="colorFont"> <?php echo $article['title']; ?></h2>
            <p>
                <?php
                echo substr($article['contents'], 0, 250); ?><a
                        href="article.php?id=<?php echo $article['id']; ?>">...Read more</a>
            </p>
            <p>
                <?php $authors = query("SELECT *
                                        FROM authors WHERE author_id='" . $article['author_id'] . "'");
                foreach ($authors as $author) {
                    echo "Author : ". $author['firstName'] . ' ' . $author['lastName'];
                } ?> &nbsp; &nbsp; &nbsp;
                <?php $categories = query("SELECT *
                                        FROM categories WHERE category_id='" . $article['category_id'] . "'");
                foreach ($categories as $category) {
                    echo "Category : " . $category['name'];
                } ?>
                &nbsp; &nbsp; &nbsp;
                <?php echo "Posted on : " . $article['creatingDate']; ?>
            </p>

        </div>
        </div>

    <?php }
}
