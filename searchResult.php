<?php include_once "functions.php";
include "parts/header.php";
include "parts/menu.php"
?>
<!-- body -->

<div class="row">
    <div class="col-9" id="contentBody">
        <div class="row">
            <div class="col-12">
                <?php

                $data = query('
                SELECT * 
                FROM articles 
                WHERE title LIKE "%' . $_GET['search'] . '%" OR contents LIKE "%' . $_GET['search'] . '%"');
                //  var_dump($data);

                ?>

                <h2 align="center"> Search results:</h2>


                <h3>
                    <?php if ($data == null)
                    {
                        echo "There are no search results! Try another keyword!";

                    }
                    else{ ?>
                </h3>

                <?php foreach ($data as $key => $article2) {
                    ?>
                        <?php

                        showArticle($article2, $key);
                        ?>
                <?php }
                ?>
            <?php } ?>
            </div>


        </div>
    </div>
    <?php include "parts/sidebar.php" ?>
</div>


<!-- end body -->
<?php include "parts/footer.php"; ?>



