<!-- body -->
<div class="row">
    <div id="contentBody" class="col-lg-9 col-sm-12" >

                <?php

                $maxPerPage = 3;
                $currentPage = 1;
                if (isset($_GET['page'])) {
                    $currentPage = $_GET['page'];
                }
                $info = query('SELECT count(*) 
                          FROM articles');


                $nrOfPages = ceil(intval($info[0]["count(*)"]) / $maxPerPage);

                $startIndex = ($currentPage - 1) * $maxPerPage;

                $articles = query('SELECT *
                        FROM articles  
                        ORDER BY creatingDate DESC 
                        LIMIT ' . $startIndex . ',' . $maxPerPage . '');
                ?>

                <?php
                foreach ($articles as $key => $article) {
                    showArticleOnIndex($article, $key);
                }


                ?>

                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <?php if ($currentPage > 1): ?>
                            <li class="page-item"><a class="page-link" href="body.php">1</a></li>
                        <?php endif; ?>
                        <?php if ($currentPage > 2): ?>
                            <li class="page-item">
                                <a class="page-link"
                                   href="body.php?page=<?php echo $currentPage - 1; ?>">Previous</a>
                            </li>
                        <?php endif; ?>
                        <li class="page-item disabled">
                            <a class="page-link" href="body.php?page=<?php echo $currentPage; ?>"
                               tabindex="-1"><?php echo $currentPage; ?></a>
                        </li>
                        <?php if ($currentPage < $nrOfPages - 1): ?>
                            <li class="page-item">
                                <a class="page-link" href="body.php?page=<?php echo $currentPage + 1; ?>">Next</a>
                            </li>
                        <?php endif; ?>
                        <?php if ($currentPage < $nrOfPages): ?>
                            <li class="page-link"><a
                                    href="body.php?page=<?php echo $nrOfPages; ?>"><?php echo $nrOfPages; ?></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </nav>

    </div>

    <!-- //sidebar-->
    <?php include "sidebar.php"; ?>

    <!-- end body -->
</div>