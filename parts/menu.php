<!-- menu -->

<div class="row" id="menuBackground">
    <div class="d-none d-sm-block">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link" href="index.php" id="menuItem">Homepage</a>
            </li>
            <?php $categories = query("SELECT *
                        FROM categories");
            foreach ($categories as $category) {
                ?>
                <li class="nav-item">
                    <a class="nav-link" href="categories.php?category_id=<?php echo $category['category_id']; ?>"
                       id="menuItem"> <?php echo $category['name']; ?> </a>
                </li>
            <?php }
            ?>

        </ul>
    </div>
    <div class="d-sm-none">
        <ul class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                   aria-expanded="false">Menu</a>
                <div class="dropdown-menu">

                    <a class="dropdown-item" href="index.php" id="menuItem">Homepage</a>
                    <div class="dropdown-divider"></div>
                    <?php $categories = query("SELECT *
                        FROM categories");
                    foreach ($categories as $category) {
                        ?>
                        <a class="dropdown-item"
                           href="categories.php?category_id=<?php echo $category['category_id']; ?>"
                           id="menuItem"> <?php echo $category['name']; ?> </a>
                    <?php }
                    ?>
                </div>
            </li>

        </ul>
    </div>

</div>

<!-- end menu -->