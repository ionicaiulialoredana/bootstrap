<!-- sidebar -->
<div id="sidebar" class="col-lg-3 d-sm-none d-lg-block ">
    <div class="row">
        <div class="col-12">

            <!--aici incepe filtrarea -->
            <?php
            if (!isset($_GET['search'])) {
                $_GET['search'] = '';
            }
            ?>

            <form class="form-inline my-2 my-lg-0" action="searchResult.php" method="get" id="formSearch">
                <input class="form-control mr-sm-2" type="text" value="<?php echo $_GET['search']; ?>" name="search"
                       placeholder="Search"/>
                <input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Search">
            </form>

            <!-- aici afisam autorii cu link-uri spre pagina de autor-->
            <h3 id="colorFont">
                <center>Authors</center>
            </h3>

            <?php
            $authors1 = query("SELECT *
                        FROM authors");

            foreach ($authors1 as $author) {
                ?>
                <h4>
                    <a href="http://188.240.210.8/web-04/iulia/proiect18bootstrap/blog/authors.php?author_id=<?php echo $author['author_id'] ?> "
                       id="sidebarItem"><?php echo $author['firstName'] . ' ' . $author['lastName']; ?></a>
                </h4>
                <?php
            }
            ?>
            <!--aici afisam ultimele 5 articole -->
            <?php
            $articleList = query("SELECT * 
FROM articles 
ORDER BY creatingDate DESC 
LIMIT 5 ");
            //var_dump($articleList);
            ?>
            <h3 id="colorFont">
                Ultimele articole
            </h3>
            <?php
            foreach ($articleList as $article) {
                ?>
                <h4>
                    <a href="http://188.240.210.8/web-04/iulia/proiect18bootstrap/blog/article.php?id=<?php echo $article['id'] ?> "
                       id="sidebarItem"><?php echo $article['title']; ?></a>

                </h4>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<!--end sidebar-->