<head>
    <title>
        Comment Admin
    </title>
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<?php
include "functions.php";

$comments = query('SELECT * 
FROM comment_admin
WHERE status="' . 'invisible' . '"
ORDER BY id ASC');

?>
    <div id="contentBody" class="w-100 p-3"><!-- this class minimizes content so that it fits the screen!  -->

        <div class="bs-example">
            <!-- Bootstrap Grid -->
            <div class="row" id="colorFont">
                <div class="col-1" id="colorFont">Comment Id</div>
                <div class="col-1" id="colorFont">Article Id</div>
                <div class="col-1" id="colorFont">Name</div>
                <div class="col-1" id="colorFont">Surname</div>
                <div class="col-4" id="colorFont">Comment</div>
                <div class="col-1" id="colorFont">Status</div>
                <div class="col-3" id="colorFont">Actions</div>
            </div>
            <?php
            foreach ($comments as $comment)
            {
            ?>
            <div class="row">
                <div class="col-1"><?php echo $comment['id'];?></div>
                <div class="col-1"><?php echo $comment['article_id'];?></div>
                <div class="col-1"> <?php echo $comment['name'];?></div>
                <div class="col-1"><?php echo $comment['surname'];?></div>
                <div class="col-4"><?php echo $comment['comment'];?></div>
                <div class="col-1"><?php echo $comment['status'];?></div>
                 <div class="col-3">
                    <div class="row">
                        <div class="col-4">
                            <a href="accept.php?id=<?php echo $comment['id']; ?>" id="colorFont">Accept</a><!-- MERGE  -->
                         </div>
                        <div class="col-4">
                            <a href="edit.php?id=<?php echo $comment['id']; ?>"id="colorFont">Edit</a><!-- MERGE  -->
                        </div>
                        <div class="col-4">
                             <a href="delete.php?id=<?php echo $comment['id']; ?>"id="colorFont">Delete</a> <!--MERGE  -->
                         </div>

                        </div>

                    </div>
            </div>
                <?php
            }
            ?>
        </div>


    </>











































</body>