<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog nou</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div id="contentBody" class="w-75 p-3">
<?php include "functions.php";
$data=queryInsert('SELECT * 
FROM comment_admin 
WHERE id='.intval($_GET['id']));

?>

    <form action="processEdit.php?id=<?php echo $_GET['id']; ?>" method="post">
        <div class="form-row">
            <div class="col">
                <label for="name">First name</label>
                <input title="editName" type="text" class="form-control" name="name" value="<?php echo $data[0]['name'];?>">
            </div>
            <div class="col">
                <label for="surname">Surname</label>
                <input title="editSurname" type="text" class="form-control" name="surname" value="<?php echo $data[0]['surname'];?>">
            </div>
        </div>
        <div class="form-group">
            <label for="exampleFormControlTextarea1">Write your comment here!</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="comment"><?php echo $data[0]['comment'];?></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit comment</button>
    </form>



</div>
</body>